using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UmbracoNet.Models;

namespace UmbracoNet.Tests
{
    [TestClass]
    public class ProductTests
    {
        [TestMethod]
        public void DownloadJsonData_ResultOk_ReturnTrue()
        {
            //Arrange

            //Act
            var prods = Product.DownloadJsonData();

            //Assert
            Assert.IsTrue(prods.ok);
        }
    }
}
