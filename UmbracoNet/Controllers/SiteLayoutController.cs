using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using UmbracoNet.Models;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;

namespace UmbracoNet.Controllers
{
    public class SiteLayoutController : SurfaceController
    {
        private const string partialViewPath = "~/Views/Partials/Shared/";               

        public ActionResult RenderHeader()
        {
            var menu = GetMenus();

            return PartialView(string.Format("{0}Header.cshtml", partialViewPath), menu);
        }

        public ActionResult RenderFooter()
        {
            return PartialView(string.Format("{0}Footer.cshtml", partialViewPath));
        }
       


        public List<MenuList> GetMenus()
        {
            int id = int.Parse(CurrentPage.Path.Split(',')[1]); //position of page?
            IPublishedContent page = Umbraco.Content(id);

            var nav = new List<MenuList>
            {
                new MenuList(new MenuLink(page.Name, page.Url))
            };
            nav.AddRange(GetSubPages(page));

            return nav;
        }

        public List<MenuList> GetSubPages(dynamic page)
        {
            List<MenuList> menus = new List<MenuList>();

            var subPages = page.Children;

            foreach(var sub in subPages)
            {
                var item = new MenuList(new MenuLink(sub.Name, sub.Url));
                menus.Add(item);
            }

            return menus;
        }

    }
}
