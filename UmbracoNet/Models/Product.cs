using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Mvc;


namespace UmbracoNet.Models
{
    public class Product
    {

        public string category { get; set; }
        public string description { get; set; }
        public int id { get; set; }
        public string image { get; set; }
        public string name { get; set; }
    }

    public class Result
    {        public List<Product> products { get; set; }
    }

    public class RootObject
    {
        public Result result { get; set; }
        public bool ok { get; set; }
    }
}
