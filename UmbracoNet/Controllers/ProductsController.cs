using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using UmbracoNet.Models;

namespace UmbracoNet.Controllers
{
    public class ProductsController : SurfaceController
    {
        private const string ViewPath = "~/Views/Products/";


        public ActionResult RenderProductsView()
        {
            var url = "https://www.jsonstore.io/3f3b001398973ab65953b73e2da267bd9a7c5400483ff8a73c75c0e1fc76075c";
            var products = DownloadJsonData(url);

            if (products.ok)
            {
                var displayed = products.result.products.Where(product => product.category == "display");
                return PartialView(string.Format("{0}ProductsMainView.cshtml", ViewPath), displayed);
            }
            else
            {
                //manage error here
                return PartialView(string.Format("home.cshtml"));
            }
        }




        private static RootObject DownloadJsonData(string url)
        {
            using (var client = new WebClient())
            {
                var json = string.Empty;
                try
                {
                    json = client.DownloadString(url);
                }
                catch (Exception) {
                    RootObject objFalse = new RootObject { result = null, ok = false };
                    return objFalse;
                }


                RootObject obj = new JavaScriptSerializer().Deserialize<RootObject>(json);

                obj.result.products.Reverse();

                foreach (Product item in obj.result.products)
                {
                    item.description = ShortenDescription(item.description, 150);
                }
                return obj;
            }
        }


        public static string ShortenDescription(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.LastIndexOf(" ", length, StringComparison.Ordinal);
            return string.Format("{0}…", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }


    }
}
