using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UmbracoNet.Models
{

        public class MenuLink
        {
            public string Text { get; set; }
            public string Url { get; set; }


            public MenuLink() { }

            public MenuLink(string text, string url)
            {
                Text = text;
                Url = url;
            }
        }

        public class MenuList
        {
            public string Text { get; set; }
            public MenuLink Link { get; set; }

            public List<MenuList> Menus { get; set; }

            public MenuList(MenuLink link)
            {
                Link = link;
            }
        }

    
}
